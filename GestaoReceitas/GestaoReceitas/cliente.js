var axios = require('axios');

function createInstance(){
    return axios.create({
        baseURL:'http://projetoarqsi2017.azurewebsites.net/api'
    });
   
}

//Create new client instance
let client = createInstance();

//Intersept any client response
client.interceptors.response.use(function(response){
    
    return response;
},handleErrors);


function handleErrors(error){
    //request that caused the error
    const originalRequest = error.config;

    if(error.response && error.response.status==401){
        
        //Authenticate the user
        return client.post('/Account/Token',{
            email:"a@a.pt",
            password:"Qwerty1!"
        })
        .then(function(response){
            
            var data = response.data;
            //Change default header
            client.defaults.headers.common.Authorization = 'Bearer '+data.token;
            //Try the request again with token
            originalRequest.headers.Authorization='Bearer '+data.token;
           
            return axios(originalRequest);
        });
    }

    //The error is not about the user authentication
    return Promise.reject(error);

}

module.exports=client;