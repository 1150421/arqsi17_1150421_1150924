// app/models/Receita.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var mongoose_validator = require("mongoose-id-validator");
var Cliente = require('../../cliente.js');
var prescricao = require('./Prescricao.js');

var receitaSchema = new Schema({
    paciente: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
    medico: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
    data: { type: Date, default: Date.now },
    prescricoes:{type:[prescricao], required: true},

});

receitaSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Receita', receitaSchema);
