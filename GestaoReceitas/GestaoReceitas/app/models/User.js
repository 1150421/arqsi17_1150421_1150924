// app/models/User.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema = new Schema({
   name:String,
   password:String,
   email:String,
   medico:Boolean,
   farmaceutico:Boolean,
   paciente:Boolean,
});

module.exports = mongoose.model('User', UserSchema);