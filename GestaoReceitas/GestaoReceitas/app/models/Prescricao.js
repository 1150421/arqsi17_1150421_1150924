var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var mongoose_validator = require("mongoose-id-validator");
var Cliente = require('../../cliente.js');


var PrescricaoSchema = new Schema({
    numero:Number,
    farmacoId:{
        type: Number,
        required: [true, 'Farmaco ID is required!'],
        validate: { //Validate is Farmaco ID exists
            validator: function(v){
                return Cliente.get(`/Farmacos/${v}`);
                
            },
        message: "Farmaco Id does not exist!"
        }
    },
    farmaco:String,
    apresentacaoId:{
        type: Number,
        required: [true, 'Apresentacao ID is required!'],
        validate: {
            validator: function(v){
                return Cliente.get(`/Apresentacoes/${v}`);
            },
        message: "Apresentacao Id does not exist!"
        }
    },
    apresentacao:String,
    posologiaId:{
        type: Number,
        required: [true, 'Posologia ID is required!'],
        validate: {
            validator: function(v){
                return Cliente.get(`/Posologias/${v}`);
            },
        message: "Posologia Id does not exist!"
        }
    },
    posologia:String,
    quantidade:Number,
    validade:{type: Date,required:true,min:Date.now},
    aviamentos:[{
        farmaceutico:{type:mongoose.Schema.Types.ObjectId,ref:'User'},
        data:Date,
        quantidade:Number
    }],

});

//Before saving data in the Database
PrescricaoSchema.pre('save', function(next) {
    var that = this;

    //Get Farmaco info
    Cliente.get(`/Farmacos/${that.farmacoId}`)
    .then(function(response){
        that.farmaco = "FarmacoId: "+response.data.id + " Nome: " +response.data.nome;
        PrescricaoSchema.obj.farmaco=that.farmaco;
        next();
    });
   
    //Get Posologia info
    Cliente.get(`/Posologias/${that.posologiaId}`)
    .then(function(response){
        that.posologia = response.data[0].posologiaMedico;
        PrescricaoSchema.obj.posologia=that.posologia;
        next();
    });
});

PrescricaoSchema.pre('save', function(next) {
    var that = this;
     //Get Apresentacao info
     Cliente.get(`/Apresentacoes/${that.apresentacaoId}`)
     .then(function(response){
         that.apresentacao = "Forma: "+response.data[0].forma + " Quantidade: " +response.data[0].quantidade+" Concentracao: "+response.data[0].concentracao;
         PrescricaoSchema.obj.apresentacao=that.apresentacao;
       
         next();
     });
});
  module.exports=PrescricaoSchema;