var bcrypt = require('bcryptjs');
var User = require('../models/User.js');
var config = require('../../config');
var jwt = require('jsonwebtoken');
var express = require('express');
var VerifyToken = require('../auth/VerifyToken');

var router = express.Router(); //Get an instance of the express router


router.post('/register',function(req,res){
    
    //Checking if the email is already registered
    User.findOne({
        email:req.body.email}, function(err,user){
            if(err) throw err;

            if(user){
                return res.status(403).json({success:false,message:'Email already in use!'});

            }else if(!user){
                
                var user=new User();
                user.name=req.body.name;     //Set the user name
                hashedPassword = bcrypt.hashSync(req.body.password,8); //encrypy password
                user.password=hashedPassword;
                user.email=req.body.email;
                user.medico=req.body.medico;
                user.farmaceutico=req.body.farmaceutico;
            
                user.save(function(err){
                    console.log("in save");
                    if(err)
                        return res.status(500).send("There was a problem registering the user.")
                    res.json({message:'User Registered'});
                })

            }


        });

    
});


//get the user's token
router.post('/login',function(req,res){
    //find the user
    User.findOne({
        email:req.body.email}, function(err,user){
            if(err) throw err;

            if(!user){
                res.json({success:false,message:'User Authentication Failed.'});

            }else if(user){
                var passwordIsValid = bcrypt.compareSync(req.body.password,user.password);
                var passwordIsValid=true;
               if(!passwordIsValid)
                    return res.status(401).send(
                        {auth:false,token:null,message:'Password Authentication Failed.'});
                //If user is found and password is right,
                //Create a token with only our given payload
                else{
                    const payload = {
                        user: user.email,
                        medico: user.medico,
                        farmaceutico: user.farmaceutico,
                        utente: user.paciente
                        };
                    var token = jwt.sign(payload, config.secret, {
                        expiresIn: 30*60 //1 hora
                    });
                    //Return the information including the token as json
                    res.json({
                        success:true,
                        message:'Enjoy your token!',
                        token:token
                    });
                }
            }


        });
});

function hasRole(userEmail,role,func){

    User.findOne({
        email:userEmail
    },function(err,user){
        if(err) throw err;

        if(!user){
            res.json({success:false,message:'Authentication failed.'});
        }else if(user){
            //check if role matches
            func(role === 'medico' && user.medico===true);
        }
    })  
}

//get all the users
router.get('/',VerifyToken, function(req,res){

    //chech if user's role matches 'medico'
    hasRole(req.user,'medico',function(decision){
        if(!decision)
            return res.status(403).send(
                {auth:false,token:null,message:'You have no authorization.'});
        else        
            User.find(function(err,users){
                if(err)
                    res.send(err);
                res.json(users);    
            })
    });
});

module.exports = router;