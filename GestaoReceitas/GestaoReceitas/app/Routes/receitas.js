var express = require('express');
var Receita = require('../models/Receita.js');
var app = require('../../app.js');
var router = express.Router(); //Get an instance of the express router
var VerifyToken = require('../auth/VerifyToken');
var jwt = require('jsonwebtoken');
var User = require('../models/User.js');
var Cliente = require('../../cliente.js');
var nodemailer = require("nodemailer");

/////////////////////
/////// GETS ////////
/////////////////////

//get Receita by id
router.get('/:id', VerifyToken, function (req, res) {
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;


    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRoleMedico === true) {
            Receita.find({ medico: userId, _id: req.params.id }, function (err, receitas) {
                console.log(receitas);
                if (err) {
                    return res.send(err);
                }
                return res.json(receitas);
            });
        } else if (userRoleFarmaceutico === true) {
            Receita.find({ _id: req.params.id }, function (err, receitas) {
                if (err) {
                    return res.send(err);
                }
                res.json(receitas);
            });
        } else if (userRolePaciente === true) {
            Receita.find({ paciente: userId, _id: req.params.id }, function (err, receitas) {
                if (err) {
                    return res.send(err);
                }
                res.json(receitas);
            });
        } else {
            res.status(403);
        }

    });

});


//Get all Prescricao of Receita 
router.get('/:id1/prescricao', VerifyToken,function (req, res) {
    var receitaId = req.params.id1;
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;

    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }
        console.log(user);
        if (userRolePaciente === true) {

            Receita.findOne({ _id: receitaId, paciente:userId }, 'prescricoes._id prescricoes.farmaco prescricoes.apresentacao prescricoes.posologia', function (err, prescricao) {
                if (err) {
                    return res.send(err);
                }
                res.json(prescricao);
            })
        } else if (userRoleMedico === true) {
            Receita.findOne({ _id: receitaId, medico:userId }, 'prescricoes._id prescricoes.farmaco prescricoes.apresentacao prescricoes.posologia', function (err, prescricao) {
                if (err) {
                    return res.send(err);
                }
                res.json(prescricao);
            })
        } else if (userRoleFarmaceutico === true) {
            Receita.findOne({ _id: receitaId }, 'prescricoes._id prescricoes.farmaco prescricoes.apresentacao prescricoes.posologia', function (err, prescricao) {
                if (err) {
                    return res.send(err);
                }
                res.json(prescricao);
            })
        } else {
            res.status(403).send("You need permissions to get the prescription");
        }
    });
});

//Get Prescricao of Receita by id
router.get('/:id1/prescricao/:id2', VerifyToken,function (req, res) {
    var receitaId = req.params.id1;
    var prescricaoId = req.params.id2;
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;

    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRolePaciente === true) {

            Receita.findOne({ _id: receitaId, paciente: userId, 'prescricoes._id': prescricaoId }, { 'prescricoes.$': 1 }, function (err, prescricao) {
                if (err) {
                    return res.send(err);
                }
                res.json("id: " + prescricao.prescricoes[0]._id + "; Apresentacao=> " + prescricao.prescricoes[0].apresentacao + "; Farmaco=> " + prescricao.prescricoes[0].farmaco + "; Posologia=> " + prescricao.prescricoes[0].posologia);
            })
        } else if (userRoleMedico === true) {
            Receita.findOne({ _id: receitaId, medico: userId, 'prescricoes._id': prescricaoId }, { 'prescricoes.$': 1 }, function (err, prescricao) {
                if (err) {
                    return res.send(err);
                }
                res.json("id: " + prescricao.prescricoes[0]._id + "; Apresentacao=> " + prescricao.prescricoes[0].apresentacao + "; Farmaco=> " + prescricao.prescricoes[0].farmaco + "; Posologia=> " + prescricao.prescricoes[0].posologia);
            })
        } else if (userRoleFarmaceutico === true) {
            Receita.findOne({ _id: receitaId, 'prescricoes._id': prescricaoId }, { 'prescricoes.$': 1 }, function (err, prescricao) {
                if (err) {
                    return res.send(err);
                }
                res.json("id: " + prescricao.prescricoes[0]._id + "; Apresentacao=> " + prescricao.prescricoes[0].apresentacao + "; Farmaco=> " + prescricao.prescricoes[0].farmaco + "; Posologia=> " + prescricao.prescricoes[0].posologia);
            })
        } else {
            res.status(403).send("You need permissions to get the prescription");
        }
    });

});



/////////////////////
/////// POSTS ///////
/////////////////////

//New Receita
router.post('/', VerifyToken, function (req, res) {

    var userRoleMedico;




    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("USER ERRO " + user);
        } else if (user) {
            console.log("USER ID" + user._id);
            userRoleMedico = user.medico;

        }

        if (userRoleMedico === true) { //Se for médico, tem permissões para criar uma receita
            var receita = new Receita();
            receita.data = req.body.data;     //Set the date
            receita.medico = req.body.medico;
            receita.paciente = req.body.paciente;
            receita.prescricoes = req.body.prescricoes;

            receita.save(function (err) {
                console.log("Saving");
                if (err) {
                    console.log(err);
                    //console.log(err);
                    return res.status(500).send("There was a problem saving the receipt.")
                }
                res.json({ message: 'Receita Registered' });
            })


            // Mail - SMTP2GO
            User.findOne({ _id: receita.paciente }, function (err, user) {
                if (err) {
                    throw err;
                } else {
                    if (!user) {
                        console.log("Invalid User");
                    } else if (user) {

                        console.log("MAIL " + user.email);

                        var smtpTransport = nodemailer.createTransport({
                            host: "mail.smtp2go.com",
                            port: 2525, // 8025, 587 and 25 can also be used. 
                            auth: {
                                user: "receitas@isep.ipp.pt",
                                pass: "4voeCnyb5QsV"
                            }
                        });

                        smtpTransport.sendMail({
                            from: "receitas@isep.ipp.pt",
                            to: user.email, //Email do paciente
                            subject: "Receitas ARQSI",
                            text: "A sua receita está agora disponivel, com o ID:  " + receita._id //Id da receita
                        }, function (error, response) {
                            if (error) {
                                console.log("mail do paciente" + response);
                                console.log(error);

                            } else {
                                console.log("Message sent: " + response.message);
                            }
                        });

                    }
                }
            })

        } else {
            return res.status(401).send({ auth: false, token: null, message: 'You need to be a doctor to prescribe a receipt.' });
        }



    });

});


//New Prescricao in Receita
router.post('/:id/prescricao', VerifyToken, function (req, res) {
    var receitaId = req.params.id;
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;

    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRoleMedico === true) {

            Receita.findById(receitaId, function (err, receita) {
                receita.prescricoes.push(req.body.prescricoes[0]);
                receita.save(function (err) {
                    if (err){
                        console.log(err);
                        return res.status(500).send("There was a problem saving the prescricao.")
                    }
                    res.json({ message: 'Prescricao Registered' });
                    
                });
            });
        } else if (userRoleFarmaceutico === true || userRolePaciente === true) {
            res.status(403).send("Only doctors can add perscriptions!");
        }
    });
});

/////////////////////
/////// PUTS ////////
/////////////////////

//Changes Prescricao of Receita
router.put('/:id1/prescricao/:id2', VerifyToken,function (req, res) {
    var receitaId = req.params.id1;
    var prescricaoId = req.params.id2;
    var i;
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;


    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRoleMedico === true) {

            //Get the receita
            Receita.findOne({ _id: receitaId, medico: userId }, function (err, receita) { //Can only be changed by the doctor who created the reciept
                if (err) {
                    return res.send(err);
                }
                //Get prescricao 
                for (i = 0; i < receita.prescricoes.length; i++) {
                    //When the prescricao is the right one and there is no Aviamentos yet
                    if (receita.prescricoes[i]._id == prescricaoId) {
                        if (receita.prescricoes[i].aviamentos[0] == null) {
                            receita.prescricoes[i].farmacoId = req.body.prescricoes[0].farmacoId;
                            receita.prescricoes[i].apresentacaoId = req.body.prescricoes[0].apresentacaoId;
                            receita.prescricoes[i].numero = req.body.prescricoes[0].numero;
                            receita.prescricoes[i].posologiaId = req.body.prescricoes[0].posologiaId;
                            receita.prescricoes[i].quantidade = req.body.prescricoes[0].quantidade;
                            receita.prescricoes[i].validade = req.body.prescricoes[0].validade;
                        } else {
                            return res.status(500).send("There is already an Aviamento in the Prescricao.");
                        }
                    }
                }
                //Save
                receita.save(function (err) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send("There was a problem saving the edited prescricao.");
                    }
                    res.status(200).send("Prescricao Edited.");
                });
            });
        } else if (userRoleFarmaceutico === true || userRolePaciente === true) {
            res.status(403).send("Only doctors can update perscriptions!");
        }
    });
});

//Aviar Prescricao
router.put('/:id1/prescricao/:id2/aviar', VerifyToken,function (req, res) {
    var receitaId = req.params.id1;
    var prescricaoId = req.params.id2;
    var numeroMedsTotal = 0;
    var numMedsAviados = 0;
    var i;
    var j;
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;

    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRoleFarmaceutico === true) {

            Receita.findById(receitaId, function (err, receita) {
                if (err) {
                    return res.send(err);
                }
                console.log(receita);
                //Get prescricao 
                for (i = 0; i < receita.prescricoes.length; i++) {
                    if (receita.prescricoes[i]._id == req.params.id2) {
                        numeroMedsTotal = receita.prescricoes[i].numero;
                        for (j = 0; j < receita.prescricoes[i].aviamentos.length; j++) {
                            //Total number of meds aviated untill now
                            numMedsAviados = numMedsAviados + receita.prescricoes[i].aviamentos[j].quantidade;
                        }
                        //If it is possible to aviar the prescricao
                        if (numMedsAviados + req.body.aviamentos[0].quantidade < numeroMedsTotal) {
                            receita.prescricoes[i].aviamentos.push(req.body.aviamentos[0]);
                            receita.save(function (err) {
                                if (err)
                                    return res.status(500).send("There was a problem Aviating the Prescricao.")
                                res.json({ message: 'Aviamento Registered' });
                            });
                        } else {
                            return res.status(500).send("Maximum number of Aviamentos in the Prescricao");
                        }
                    }
                }

            });
        } else if (userRoleMedico === true || userRolePaciente === true) {
            res.status(403).send("This action requires Farmaceutico permissions!");
        }
    });
});


router.put('/:id', VerifyToken, function (req, res) {
    var receitaPrescricoesSize;
    var prescricao;
    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;

    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRoleMedico === true) {

            Receita.findOne({ _id: req.params.id, medico: userId }, function (err, receita) {

                if (err) res.send(err);
                receitaPrescricoesSize = receita.prescricoes.length;
                for (i = 0; i < req.body.prescricoes.length; i++) {
                    if (i >= receitaPrescricoesSize) {
                        receita.prescricoes.push(req.body.prescricoes[i]);
                    } else if (receita.prescricoes[i].aviamentos.length == 0) {
                        receita.prescricoes[i].numero = req.body.prescricoes[i].numero;
                        receita.prescricoes[i].farmacoId = req.body.prescricoes[i].farmacoId;
                        receita.prescricoes[i].apresentacaoId = req.body.prescricoes[i].apresentacaoId;
                        receita.prescricoes[i].posologiaId = req.body.prescricoes[i].posologiaId;
                        receita.prescricoes[i].quantidade = req.body.prescricoes[i].quantidade;
                        receita.prescricoes[i].validade = req.body.prescricoes[i].validade;

                    }
                }
                receita.save(function (err) {
                    console.log("Saving");
                    if (err) {
                        console.log(err);
                        return res.status(500).send("There was a problem updating the receipt.")
                    } else {
                        return res.status(200).json({ message: 'Receita Updated' });
                    }
                })


            })
        } else if (userRoleFarmaceutico === true) {
            Receita.findOne({ _id: req.params.id }, function (err, receita) {

                if (err) res.send(err);
                receitaPrescricoesSize = receita.prescricoes.length;
                receita.medico = req.body.medico;
                receita.paciente = req.body.paciente;
                for (i = 0; i < req.body.prescricoes.length; i++) {

                    if (receita.prescricoes[i].aviamentos.length < req.body.prescricoes[i].aviamentos.length) {
                        receita.prescricoes[i].aviamentos.push(req.body.prescricoes[i].aviamentos);
                    }

                    for (k = 0; k < req.body.prescricoes[i].aviamentos.length; k++) {
                        receita.prescricoes[i].aviamentos[k].farmaceutico = req.body.prescricoes[i].aviamentos[k].farmaceutico;
                        receita.prescricoes[i].aviamentos[k].data = req.body.prescricoes[i].aviamentos[k].data;
                        receita.prescricoes[i].aviamentos[k].quantidade = req.body.prescricoes[i].aviamentos[k].quantidade;
                    }

                }
                receita.save(function (err) {
                    console.log("Saving");
                    if (err) {
                        console.log(err);
                        return res.status(500).send("There was a problem updating the receipt.")
                    } else {
                        return res.status(200).json({ message: 'Receita Updated' });
                    }
                })
            })

        } else if (userRolePaciente === true) {
            return res.status(403).send("Paciente cant update a reciept");
        }
      
    });
});

router.delete('/:id', VerifyToken, function (req, res) {

    var userId;
    var userRoleMedico;
    var userRoleFarmaceutico;
    var userRolePaciente;


    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRoleMedico = user.medico;
            userRoleFarmaceutico = user.farmaceutico;
            userRolePaciente = user.paciente;
        }

        if (userRoleMedico === true || userRoleFarmaceutico === true || userRolePaciente === true) {
            res.status(403).send({ message: 'You dont have permissions to delete a receipt.' });
        } else {
            res.send("You cannot delet a reciept!");
        }

    });

});
module.exports = router;

