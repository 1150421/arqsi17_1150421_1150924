var express = require('express');
var Receita = require('../models/Receita.js');
var app = require('../../app.js');
var router = express.Router(); //Get an instance of the express router
var VerifyToken = require('../auth/VerifyToken');
var jwt = require('jsonwebtoken');
var User = require('../models/User.js');
var Cliente = require('../../cliente.js');



//Get Prescricoes por Aviar of Paciente
router.get('/:id1/prescricao/poraviar/:data?', VerifyToken,function (req, res) {

    var utenteId = req.params.id1;
    var receita;
    var numAviamentos = 0;
    var prescricoesDoUtente = [];
    var dataConvertida;
    var date;
    var dateParts;
    var i;
    var j;
    var k;
    var userRolePaciente;
    var userId;

    User.findOne({ email: req.user }, function (err, user) {
        if (err) throw err;

        if (!user) {
            console.log("ERROR");
        } else if (user) {
            console.log("USER ID" + user._id);
            userId = user._id;
            userRolePaciente = user.paciente;
        }

        if (userRolePaciente === true && userId ===utenteId) { //Only the logged patient can access this info

            //Receitas of paciente
            Receita.find({ paciente: utenteId }, function (err, receitas) {

                if (err) {
                    return res.send(err);
                }
                //Receitas
                for (j = 0; j < receitas.length; j++) {
                    receita = receitas[j];
                    //Prescricoes of receita
                    for (i = 0; i < receita.prescricoes.length; i++) {
                        //When the prescricao is valid
                        // dataConvertida = DateTime.Parse(req.params.data);
                        dateParts = req.params.data.split("-");
                        date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
                        if ((req.params.data && receita.prescricoes[i].validade <= date) || (!req.params.data)) {
                            for (k = 0; k < receita.prescricoes[i].aviamentos.lenght; k++) {

                                numAviamentos = numAviamentos + receita.prescricoes[i].aviamentos[k].quantidade;
                            }
                            //Prescricao por Aviar
                            if (numAviamentos < receita.prescricoes[i].quantidade) {

                                prescricoesDoUtente.push({ prescricoes: receita.prescricoes[i] });
                            }
                            numAviamentos = 0;
                        }
                    }
                }
                res.json(prescricoesDoUtente);
            });
        } else {
            res.status(403).send("You dont have permissions!");
        }
    });
});
module.exports = router;