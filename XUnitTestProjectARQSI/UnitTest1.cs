using ProjetoARQSI.Controllers;
using ProjetoARQSI.Models;
using System;
using System.Linq;
using Xunit;

namespace XUnitTestProjectARQSI
{
    public class UnitTest1
    {
        private readonly ProjetoARQSIContext _context;

        [Fact]
        public void Test1()
        {
            var controller = new MedicamentosController(_context);
            var result = controller.GetMedicamento();
            Assert.Equal(2, result.Count());
        }
    }
}
