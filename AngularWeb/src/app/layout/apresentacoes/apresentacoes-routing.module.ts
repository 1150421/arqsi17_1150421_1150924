import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApresentacoesComponent } from './apresentacoes.component';

const routes: Routes = [
    {
        path: '', component: ApresentacoesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApresentacoesRoutingModule {
}