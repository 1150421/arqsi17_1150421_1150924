import { Component, OnInit } from '@angular/core';
import { Apresentacao } from '../../models/Apresentacao/apresentacao';
import { ApresentacoesService } from '../../services/apresentacoes.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-apresentacoes',
  templateUrl: './apresentacoes.component.html',
  styleUrls: ['./apresentacoes.component.scss']
})
export class ApresentacoesComponent implements OnInit {
  apresentacoes: Apresentacao[] = [];
  loading:boolean;
  constructor(private apresentacoesService: ApresentacoesService, private http: HttpClient) { }

  ngOnInit() {
    this.loading=true;
    this.apresentacoesService.getApresentacoes()
      .subscribe(apresentacoes => {
        this.apresentacoes = apresentacoes;
        console.log(apresentacoes);
        this.loading=false;
      })
  }

}