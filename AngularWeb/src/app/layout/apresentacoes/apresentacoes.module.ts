import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { ApresentacoesRoutingModule } from './apresentacoes-routing.module';
import { ApresentacoesComponent } from './apresentacoes.component';
//import { StatModule } from '../../shared';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        ApresentacoesRoutingModule,
        FormsModule
    ],
    declarations: [
        ApresentacoesComponent ]
})
export class ApresentacoesModule {}