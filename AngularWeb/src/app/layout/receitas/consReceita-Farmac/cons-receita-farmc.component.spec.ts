import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsReceitaFarmComponent } from './cons-receita-farmc.component';

describe('CriarReceitaComponent', () => {
  let component: ConsReceitaFarmComponent;
  let fixture: ComponentFixture<ConsReceitaFarmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsReceitaFarmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsReceitaFarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
