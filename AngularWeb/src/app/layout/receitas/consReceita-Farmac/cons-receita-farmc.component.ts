import { Component, OnInit, Input } from '@angular/core';
import { Receita } from '../../../models/Receita/receita';
import { ReceitasService } from '../../../services/receitas.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../../../services/authentication.service';
import { User } from '../../../models/User/user';

@Component({
  selector: 'app-cons-receitas-farm',
  templateUrl: './cons-receita-farmc.component.html',
  styleUrls: ['./cons-receita-farmc.component.scss']
})
export class ConsReceitaFarmComponent implements OnInit {
  model: any = {};
  selectedReceita: Receita;
  userInfo: User;
  loading: boolean;
  aviarP: boolean;
  prescricaoNumero: any;
  error='';
  prescricaoId:any;
  constructor(private receitaService: ReceitasService, private http: HttpClient) { }

  private receitasUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/receita';

  ngOnInit() {
    this.loading = false;
    this.aviarP = false;
    /*  this.receitaService.getReceitas()
       .subscribe(receitas => {
         this.receitas = receitas;
       }) */
  }

  selectReceita() {
    this.getReceitaById(this.model.receitaId);
  }

  getReceitaById(receitaId: String) {
    this.receitaService.getReceitaById(receitaId)
      .subscribe(receita => {

        this.selectedReceita = receita;
        this.loading = true;
      })
  }

  aviar(prescricaoNumero: any,prescricaoId:any) {
    this.aviarP = true;
    this.prescricaoNumero = prescricaoNumero;
    this.prescricaoId=prescricaoId;
  }

  aviarQuantidade() {

    this.receitaService.aviarPrescricao(this.selectedReceita._id, this.prescricaoNumero,this.model.aviarQuantidade)
    .subscribe(result => {
      this.aviarP=false;
      if (result === true) {
          console.log("aviou!!");
      } else {
          this.error = 'Erro ao aviar Prescricao';
      }
  });
  }

  aviarTotalmente() {
    let quantTotalAviamentos=0;
    let maxAviamentos;
    for(let prescricao of this.selectedReceita.prescricoes){
      if(prescricao._id==this.prescricaoId){
        maxAviamentos=prescricao.quantidade;
        for(let aviamento of prescricao.aviamentos){
          quantTotalAviamentos=quantTotalAviamentos+aviamento.quantidade;
        }
      }
    }

    const numAviar = maxAviamentos-quantTotalAviamentos;
    this.receitaService.aviarPrescricao(this.selectedReceita._id, this.prescricaoNumero,numAviar)
    .subscribe(result => {
      this.aviarP=false;
      if (result === true) {
          console.log("aviou!!");
      } else {
          this.error = 'Erro ao aviar Prescricao';
      }
  });
  }
}
