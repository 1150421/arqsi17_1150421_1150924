import { Component, OnInit } from '@angular/core';
import { Receita } from '../../../models/Receita/receita';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { UserData } from '../../../models/User/userData';
import { Prescricao } from '../../../models/Prescricoes/prescricoes';
import { Farmaco } from '../../../models/Receita/farmaco';
import { GMService } from '../../../services/gestaoMedicamentos.sevice';
import { Apresentacao } from '../../../models/Apresentacao/apresentacao';
import { ReceitasService } from '../../../services/receitas.service';

@Component({
  selector: 'app-criar-receita',
  templateUrl: './criar-receita.component.html',
  styleUrls: ['./criar-receita.component.scss']
})

export class CriarReceitaComponent implements OnInit {
  utentes: UserData[] = [];
  receitas: Receita[] = [];
  farmacos: Farmaco[];
  apresentacoes: Apresentacao[];
  model: any = {};
  prescricoes:any= [];
  loading: boolean;
  error = '';
  medicoId: any = {};

  constructor(private receitaService: ReceitasService, private userService: UserService, private gmService: GMService) { }

  ngOnInit() {
    this.loading = true;
    this.userService.getUtentes()
      .subscribe(utentes => {
        this.utentes = utentes;
      })

    this.gmService.getFarmacos()
      .subscribe(farmacos => {
        this.farmacos = farmacos;
        
      })

    this.gmService.getApresentacoes()
      .subscribe(apresentacoes => {
        this.apresentacoes = apresentacoes;
        this.loading = false;
      })

  }

  addPrescricao() {
      const farmaco_id= this.model.farmaco;
      const apresentacao_id= this.model.apresentacao;
      const data = new Date(this.model.validade);
      const day = data.getDate();
      const monthIndex =data.getMonth();
      const year = data.getFullYear();
      const validade = (monthIndex+1)+"-"+day+"-"+year;
     // const posologia= this.model.posologia;
      const quantidade= this.model.quantidade;
      const posologia_id=1;
      const numeroPrescricao =1;
      this.prescricoes.push({ numeroPrescricao,farmaco_id, apresentacao_id,posologia_id, quantidade,validade });
  }

  removerPrescricao(index: any) {
    this.prescricoes.splice(index, 1);
  }
  //Criar Receita
  onSubmit() {
    const data = new Date(Date.now());
    const day = data.getDate();
    const monthIndex = data.getMonth();
    const year = data.getFullYear();
    const myFormattedDate = (monthIndex+1)+"-"+day+"-"+year;
    this.medicoId = JSON.parse(localStorage.getItem('userInfo')).id;
    this.removerPrescricao(0);
    console.log(this.prescricoes);
    this.receitaService.postReceita(this.model.utente, this.medicoId, data, this.prescricoes)
      .subscribe(result => {

        if (result === true) {
          console.log("criou!");
        } else {
          this.error = 'Error registering';
        }
      });
  }
}
