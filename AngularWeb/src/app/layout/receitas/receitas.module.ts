import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { ReceitasRoutingModule } from './receitas-routing.module';
import { ReceitasComponent } from './receitas.component';
import { ReceitaDetailComponent } from './receita-detail.component';
import { CriarReceitaComponent } from './criar-receita/criar-receita.component';
import { SharedPipesModule } from '../../shared';
import{ConsReceitaFarmComponent} from './consReceita-Farmac/cons-receita-farmc.component';
import { AtualizarReceitaComponent } from './atualizar-receita/atualizar-receita.component';
//import { StatModule } from '../../shared';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        ReceitasRoutingModule,
        FormsModule,
        SharedPipesModule
    ],
    declarations: [
        ReceitasComponent,
        ReceitaDetailComponent,
        CriarReceitaComponent,
        ConsReceitaFarmComponent,
        AtualizarReceitaComponent
        
    ]
})
export class ReceitasModule {}
