import { Component, OnInit } from '@angular/core';
import { Receita } from '../../models/Receita/receita';
import { ReceitasService } from '../../services/receitas.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/User/user';

@Component({
  selector: 'app-receitas',
  templateUrl: './receitas.component.html',
  styleUrls: ['./receitas.component.scss']
})
export class ReceitasComponent implements OnInit {
  receitas: Receita[] = [];
  selectedReceita: Receita;
  subscriptionAuth: Subscription;
  userInfo: User;
  constructor(private authenticationService: AuthenticationService, private receitaService: ReceitasService, private http: HttpClient) { }

  ngOnInit() {
    this.userInfo = this.authenticationService.userInfo;

    this.receitaService.getReceitas()
      .subscribe(receitas => {
        this.receitas = receitas;
      })
  }
  onSelect(receita: Receita): void {
    this.selectedReceita = receita;
  }

}
