import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceitasComponent } from './receitas.component';
import { CriarReceitaComponent } from './criar-receita/criar-receita.component';
import { ConsReceitaFarmComponent } from './consReceita-Farmac/cons-receita-farmc.component';
import{AtualizarReceitaComponent} from './atualizar-receita/atualizar-receita.component';
const routes: Routes = [
    {
        path: '', component: ReceitasComponent
    },
    {
        path: 'criar-receita', component: CriarReceitaComponent
    },
    {
        path: 'cons-receita-farmc', component: ConsReceitaFarmComponent
    },
    {
        path: 'atualizar-receita', component: AtualizarReceitaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReceitasRoutingModule {
}
