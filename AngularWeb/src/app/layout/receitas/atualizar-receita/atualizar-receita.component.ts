import { Component, OnInit } from '@angular/core';
import { Receita } from '../../../models/Receita/receita';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ReceitasService } from '../../../services/receitas.service';
import { UserService } from '../../../services/user.service';
import { UserData } from '../../../models/User/userData';
import { Prescricao } from '../../../models/Prescricoes/prescricoes';
import { Farmaco } from '../../../models/Receita/farmaco';
import { GMService } from '../../../services/gestaoMedicamentos.sevice';
import { AuthenticationService } from '../../../services/authentication.service';
import { Apresentacao } from '../../../models/Apresentacao/apresentacao';
@Component({
  selector: 'app-atualizar-receita',
  templateUrl: './atualizar-receita.component.html',
  styleUrls: ['./atualizar-receita.component.scss']
})

@Injectable()
export class AtualizarReceitaComponent implements OnInit {
  utentes: UserData[] = [];
  selectedReceita: Receita;
  receitas: Receita[] = [];
  farmacos: Farmaco[];
  apresentacoes:Apresentacao[];
  loading: boolean;
  model: any = {};
  prescricoes = [];
  numeroPrescricao: any;
  prescricaoId: any;
  editarFlag: boolean;
  result:boolean;
  error='';
  loading2:boolean;
  constructor( private userService: UserService, private gmService: GMService, private receitaService: ReceitasService) { }

  ngOnInit() {
    this.loading = false;
    this.loading2 = false;
    this.editarFlag = false;
    this.result=false;
    this.userService.getUtentes()
      .subscribe(utentes => {
        this.utentes = utentes;
      })

    this.gmService.getFarmacos()
      .subscribe(farmacos => {
        this.farmacos = farmacos;
      })
      this.gmService.getApresentacoes()
      .subscribe(apresentacoes => {
        this.apresentacoes = apresentacoes;
        this.loading2=true;
      })
  }

  selectReceita() {
    this.getReceitaById(this.model.receitaId);
  }

  getReceitaById(receitaId: String) {
    this.receitaService.getReceitaById(receitaId)
      .subscribe(receita => {

        this.selectedReceita = receita;
        this.loading = true;
      })
  }


  editar(numeroPrescricao: any, prescricaoId: any) {
    this.numeroPrescricao = numeroPrescricao;
    this.prescricaoId = prescricaoId;
    this.editarFlag = true;
  }

  submeterEdicao() {
    const data = new Date(this.model.validade);
    const day = data.getDate();
    const monthIndex = data.getMonth();
    const year = data.getFullYear();
    const validade = (monthIndex + 1) + "-" + day + "-" + year;
    console.log(this.model.quantidade);
    this.receitaService.putPrescricao(this.selectedReceita._id, this.numeroPrescricao, this.model.quantidade,validade).subscribe(result => {

      if (result === true) {
        console.log("ALTEROU");
      } else {
        this.error = 'Username or password is incorrect';
      }
    })
  }

}
