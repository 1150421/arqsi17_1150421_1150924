import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '../shared/guard/auth.guard';
import { MedicoGuard } from '../shared/guard/medico.guard';
import { ReceitasComponent } from './receitas/receitas.component';
import { UtenteGuard } from '../shared/guard/utente.guard';
import { FarmaceuticoGuard } from '../shared/guard/farmaceutico.guard';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: 'receitas', loadChildren: './receitas/receitas.module#ReceitasModule', canActivate: [AuthGuard] },
           
            { path: 'apresentacoes', loadChildren: './apresentacoes/apresentacoes.module#ApresentacoesModule'},
            {path:'comentar-apresentacao-farmaco', loadChildren: './comentar-apresentacao-farmaco/comentar-apresentacao-farmaco.module#ComentarApresentacaoFarmacoModule',canActivate: [AuthGuard] }
      
        ]
    }
];

@NgModule({
    imports: [FormsModule,RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
