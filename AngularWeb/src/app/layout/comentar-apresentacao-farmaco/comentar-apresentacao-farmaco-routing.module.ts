import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComentarApresentacaoFarmacoComponent } from './comentar-apresentacao-farmaco.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: '', component: ComentarApresentacaoFarmacoComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule]
})
export class ComentarApresentacaoFarmacoRouting {
}