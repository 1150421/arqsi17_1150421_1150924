import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { ComentarApresentacaoFarmacoRouting } from './comentar-apresentacao-farmaco-routing.module';
import { ComentarApresentacaoFarmacoComponent } from './comentar-apresentacao-farmaco.component';
//import { StatModule } from '../../shared';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        ComentarApresentacaoFarmacoRouting,
        FormsModule
    ],
    declarations: [ComentarApresentacaoFarmacoComponent]
})
export class ComentarApresentacaoFarmacoModule{}