import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentarApresentacaoFarmacoComponent } from './comentar-apresentacao-farmaco.component';

describe('ComentarApresentacaoFarmacoComponent', () => {
  let component: ComentarApresentacaoFarmacoComponent;
  let fixture: ComponentFixture<ComentarApresentacaoFarmacoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentarApresentacaoFarmacoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentarApresentacaoFarmacoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
