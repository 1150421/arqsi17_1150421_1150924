import { Component, OnInit } from '@angular/core';
import {GMService} from '../../services/gestaoMedicamentos.sevice';
import { HttpClient } from '@angular/common/http';
import{Farmaco} from '../../models/Receita/farmaco';
import { Apresentacao } from '../../models/Apresentacao/apresentacao';

@Component({
  selector: 'app-comentar-apresentacao-farmaco',
  templateUrl: './comentar-apresentacao-farmaco.component.html',
  styleUrls: ['./comentar-apresentacao-farmaco.component.scss']
})

export class ComentarApresentacaoFarmacoComponent implements OnInit {
  farmacos:Farmaco[];
  model:any={};
  apresentacao:Apresentacao;
  apresentacoes:Apresentacao[];
  loading:boolean;
  loadingF:boolean;

  private farmacoUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/farmacos/$(model.farmaco.nome)';

  constructor(private gmService: GMService, private http: HttpClient) { }
  
    ngOnInit() {
      this.loading=true;
      this.loadingF=true;
      this.gmService.getFarmacos()
        .subscribe(farmacos => {
          this.farmacos = farmacos;
          this.loadingF=false;
          this.getApresentacoes(this.model.farmaco);
        })
    }
    
    getApresentacoes(farmaco:string){
      this.gmService.getApresentacoesFar().subscribe(apresentacoes => {
        this.apresentacoes = apresentacoes;
       // this.apresentacoes.filter(apresentacao=>apresentacao.farmaco===farmaco);
        this.loading=false;
      });
      
    }

    onSelect(apresentacao:Apresentacao){
      this.apresentacao=apresentacao;
    }

   addComentario():void{
      this.apresentacao.comentario=this.model.comentario;
   }
}
