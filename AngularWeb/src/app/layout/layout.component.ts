import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/User/user';
import { Injectable } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };
  public subscriptionAuth: Subscription;
  public userInfo: User;

   constructor(
    public authenticationService: AuthenticationService) { }
  ngOnInit() {
    this.userInfo = this.authenticationService.userInfo;
    
  }
}
