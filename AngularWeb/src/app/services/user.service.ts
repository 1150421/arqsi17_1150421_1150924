import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { UserData } from '../models/User/userData';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class UserService {

  private userUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/users';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }
  
    getUtentes(): Observable<UserData[]> {
    return this.http.get<UserData[]>(`${this.userUrl}?role=utente`, this.getHeaders());
   }
    getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
      this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
