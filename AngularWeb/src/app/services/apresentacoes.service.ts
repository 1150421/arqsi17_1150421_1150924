import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Apresentacao } from '../models/Apresentacao/apresentacao';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class ApresentacoesService {

    private apresentacoesUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/Medicamentos';
    //verificar o servidor:porta
    constructor(
        private http: HttpClient,
        private authenticationService: AuthenticationService) { }

    getApresentacoes(): Observable<Apresentacao[]> {
        return this.http.get<Apresentacao[]>(this.apresentacoesUrl);
    }
   /*  getHeaders() {
        let headers = new HttpHeaders({
            'x-access-token':
                this.authenticationService.userInfo.token
        });

        let httpOptions = {
            headers: headers
        };
        return httpOptions;
    } */
}