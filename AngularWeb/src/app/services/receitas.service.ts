import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Receita } from '../models/Receita/receita';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class ReceitasService {

  private receitasUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/receita';
  private aviarUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/receita';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { 
      
    }
    
  getReceitas(): Observable<Receita[]> {
    return this.http.get<Receita[]>(this.receitasUrl, this.getHeaders());
  }
  getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
  getReceitaById(receitaId: String): Observable<Receita> {
    
    const url = `${this.receitasUrl}/${receitaId}`;
    return this.http.get<Receita>(url, this.getHeaders());
  }

  postReceita(utente: string,  medico: string,data: Date,prescricoes: any): Observable<boolean> {
    
    return new Observable<boolean>(observer => {
      this.http.post(this.receitasUrl,{
        utente: utente,
        data: data,
        medico: medico,
        prescricoes: prescricoes
      },this.getHeaders())
        .subscribe(data => {
          observer.next(true);
        });

    });
  }
  aviarPrescricao(receitaId: any,  prescricaoNumero: any,quantidade:any): Observable<boolean> {
    const url = `${this.aviarUrl}/${receitaId}/prescricao/${prescricaoNumero}/aviar`;
    
    return new Observable<boolean>(observer => {
      this.http.post(url,{
        quantidade: quantidade,
      },this.getHeaders())
        .subscribe(data => {
          observer.next(true);
        });

    });
  }

  putPrescricao(receitaId:any,prescricaoNumero:any,quantidade:any,validade:any):Observable<boolean>{
    const url = `${this.aviarUrl}/${receitaId}/prescricao/${prescricaoNumero}`;
    
    return new Observable<boolean>(observer => {
      this.http.put(url,{
        quantidade:quantidade,
        validade:validade
      },this.getHeaders())
        .subscribe(data => {
          observer.next(true);
        });

    });

  }
}