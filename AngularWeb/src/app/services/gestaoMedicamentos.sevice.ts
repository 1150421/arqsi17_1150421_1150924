import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Farmaco } from '../models/Receita/farmaco';
import { AuthenticationService } from './authentication.service';
import{Apresentacao} from '../models/Apresentacao/apresentacao';

@Injectable()
export class GMService {
  farmacos:Farmaco[];
  private farmacosUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/farmacos';
  private apresentacaoUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/apresentacoes';
  private medUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/medicamentos';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }
  
    getFarmacos(): Observable<Farmaco[]> {
    return this.http.get<Farmaco[]>(this.farmacosUrl, this.getHeaders());
   }

   getApresentacoes(): Observable<Apresentacao[]> {
    return this.http.get<Apresentacao[]>(this.apresentacaoUrl, this.getHeaders());
   }
   getApresentacoesFar(): Observable<Apresentacao[]> {
    return this.http.get<Apresentacao[]>(this.medUrl, this.getHeaders());
   }
    getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
      this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }

  
}
