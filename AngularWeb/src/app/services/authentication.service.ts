import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from
    '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import * as jwt_decode from 'jwt-decode';
import { User } from '../models/User/user';
import { Router, NavigationEnd } from '@angular/router';
class Token { token: string };

@Injectable()
export class AuthenticationService {

    //private authUrl = 'http://localhost:8080/api/users/login';
    private authUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/users/login';
    //verificar o servidor:porta
    public userInfo: User;
    authentication: Subject<User> = new Subject<User>();
    constructor(
        private http: HttpClient, public router: Router
    ) {
        console.log('CONSTRUCTOR OF AUTH SERVICE');
        this.userInfo = localStorage.userInfo && JSON.parse(localStorage.userInfo);
    }
    login(email: string, password: string): Observable<boolean> {
        return new Observable<boolean>(observer => {
            this.http.post<Token>(this.authUrl, {
                email: email,
                password: password
            })
                .subscribe(data => {
                    console.log('logged in');
                    if (data.token) {
                        const tokenDecoded = jwt_decode(data.token);
                        this.userInfo = {
                            token: data.token,
                            tokenExp: tokenDecoded.exp,
                            medico: tokenDecoded.medico,
                            farmaceutico: tokenDecoded.farmaceutico,
                            utente: tokenDecoded.utente,
                            id: tokenDecoded.id
                        }

                        localStorage.userInfo = JSON.stringify(this.userInfo);

                        //this.authentication.next(this.userInfo);
                        observer.next(true);
                    } else {
                        //this.authentication.next(this.userInfo);
                        observer.next(false);
                    }
                },
                (err: HttpErrorResponse) => {
                    if (err.error instanceof Error) {
                        console.log("Client-side error occured.");
                    } else {
                        console.log("Server-side error occured.");
                    }
                    console.log(err);
                    this.authentication.next(this.userInfo);
                    observer.next(false);
                });

        });
    }
    logout() {
        this.userInfo = null;
        localStorage.removeItem('userInfo');
        //this.authentication.next(this.userInfo);
        console.log('Redirecting', this.userInfo);
        this.router.navigate(['/login']);
    }

    isAuthenticated(): boolean {
        console.log('is authenticated? ', this.userInfo);
        if (this.userInfo == null) {
            return false;
        }
        let expirationDate = this.userInfo.tokenExp * 1000;
        return new Date().getTime() <= expirationDate;
    }
}

