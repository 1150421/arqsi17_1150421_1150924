import { TestBed, inject } from '@angular/core/testing';

import { GMService } from './gestaoMedicamentos.sevice';

describe('ReceitasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GMService]
    });
  });

  it('should be created', inject([GMService], (service: GMService) => {
    expect(service).toBeTruthy();
  }));
});
