import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import {AuthenticationService} from './services/authentication.service';
import {ReceitasService} from './services/receitas.service';
import { MedicoGuard } from './shared/guard/medico.guard';
import { FarmaceuticoGuard } from './shared/guard/farmaceutico.guard';
import { UtenteGuard } from './shared/guard/utente.guard';
import { UserService } from './services/user.service';
import {GMService} from './services/gestaoMedicamentos.sevice';
import{ApresentacoesService} from './services/apresentacoes.service';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-5/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule
    ],
    declarations: [AppComponent],
    providers: [AuthenticationService,GMService,ReceitasService,AuthGuard,MedicoGuard,UtenteGuard,FarmaceuticoGuard, UserService,ApresentacoesService],

    bootstrap: [AppComponent]
})
export class AppModule {}