import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpErrorResponse } from
'@angular/common/http';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
    private authUrl = 'http://gestaoreceitas1140257-1141187.azurewebsites.net/api/users/register';

    constructor(  private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {}

    onSubmit(){
        this.loading = true;
        this.register(this.model.nome,this.model.email,
            this.model.password)
            .subscribe(result => {
                this.loading = false;
                if (result === true) {
                    this.router.navigate(['/login']);
                } else {
                    this.error = 'Error registering';
                }
            });
    }

    register(nome:String,email:String,password:String):Observable<boolean>{
        return new Observable<boolean>(observer => {
            this.http.post(this.authUrl, {
              email: email,
              password: password,
              name:nome,
              medico:false,
              farmaceutico:false,
              utente:true
            })
              .subscribe(data => {
                observer.next(true);
              },
              (err: HttpErrorResponse) => {
                if (err.error instanceof Error) {
                  console.log("Client-side error occured.");
                } else {
                  console.log("Server-side error occured.");
                }
                console.log(err);
                observer.next(false);
              });
      
          });

    }
}
