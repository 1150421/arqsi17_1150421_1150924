﻿using ProjetoARQSI.Models;
using System;
using System.Linq;

namespace ProjetoARQSI.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ProjetoARQSIContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Farmaco.Any())
            {
                return;   // DB has been seeded
            }

            var farmacos = new Farmaco[]
            {
            new Farmaco{Nome="Ipobrufeno"},
            new Farmaco{Nome="Farmaco2"},
            new Farmaco{Nome="Farmaco3"},
            new Farmaco{Nome="Farmaco4"},

            };
            foreach (Farmaco s in farmacos)
            {
                context.Farmaco.Add(s);
            }
            context.SaveChanges();

            var apresentacoes = new Apresentacao[]
            {
            new Apresentacao{Forma="Xarope",Quantidade="100ml",Concentracao=20,farmacoId=1},
            new Apresentacao{Forma="Comprimidos",Quantidade="Caixa de 40",Concentracao=10,farmacoId=4},
            new Apresentacao{Forma="Xarope",Quantidade="200ml",Concentracao=15,farmacoId=2},
            new Apresentacao{Forma="Supositorio",Quantidade="Caixa de 15",Concentracao=69,farmacoId=3},
            };
            foreach (Apresentacao c in apresentacoes)
            {
                context.Apresentacao.Add(c);
            }
            context.SaveChanges();

            var medicamentos = new Medicamento[]
            {
            new Medicamento{Nome="Brufen",ApresentacaoId=1},
            new Medicamento{Nome="Ben-u-ron",ApresentacaoId=1},
            new Medicamento{Nome="Med1",ApresentacaoId=2},
            new Medicamento{Nome="Med2",ApresentacaoId=3},
            new Medicamento{Nome="Med3",ApresentacaoId=4},

            };
            foreach (Medicamento e in medicamentos)
            {
                context.Medicamento.Add(e);
            }
            context.SaveChanges();

            var posologias = new Posologia[]
          {
            new Posologia{PosologiaHabitual="2 vezes ao dia",PosologiaMedico="23 vezes ao dia",ApresentacaoId=1},
            new Posologia{PosologiaHabitual="3 vezes ao dia",PosologiaMedico="1 vezes ao dia",ApresentacaoId=2},
            new Posologia{PosologiaHabitual="4 vezes ao dia",PosologiaMedico="0 vezes ao dia",ApresentacaoId=3},
            new Posologia{PosologiaHabitual="24 vezes ao dia",PosologiaMedico="100 vezes ao dia",ApresentacaoId=4},


          };
            foreach (Posologia e in posologias)
            {
                context.Posologia.Add(e);
            }
            context.SaveChanges();
        }
    }
}