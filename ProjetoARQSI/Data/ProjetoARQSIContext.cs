﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetoARQSI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ProjetoARQSI.Models
{
    public class ProjetoARQSIContext : IdentityDbContext<UserEntity>
    {
        public ProjetoARQSIContext (DbContextOptions<ProjetoARQSIContext> options)
            : base(options)
        {
        }

        public DbSet<ProjetoARQSI.Models.Medicamento> Medicamento { get; set; }

        public DbSet<ProjetoARQSI.Models.Farmaco> Farmaco { get; set; }

        public DbSet<ProjetoARQSI.Models.Posologia> Posologia { get; set; }

        public DbSet<ProjetoARQSI.Models.Apresentacao> Apresentacao { get; set; }
    }
}
