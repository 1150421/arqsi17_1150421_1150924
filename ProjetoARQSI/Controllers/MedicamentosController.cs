﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoARQSI.Models;
using Microsoft.AspNetCore.Authorization;


namespace ProjetoARQSI.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Medicamentos")]
    public class MedicamentosController : Controller
    {
        private readonly ProjetoARQSIContext _context;


        public MedicamentosController(ProjetoARQSIContext context)
        {
            _context = context;
        }

        //Method to create DTO
        private static readonly Expression<Func<Medicamento, MedicamentoDTO>> AsMedicamentoDTO
            = x => new MedicamentoDTO
            {
                id = x.id,
                Nome = x.Nome,
                apresentacao = new ApresentacaoDTO { Forma = x.Apresentacao.Forma, Quantidade = x.Apresentacao.Quantidade, Concentracao = x.Apresentacao.Concentracao, farmacoNome = x.Apresentacao.Farmaco.Nome },

            };

        //Method to create DTO
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDTO
            = x => new ApresentacaoDTO
            {
                Forma = x.Forma,
                Quantidade = x.Quantidade,
                Concentracao = x.Concentracao,
                farmacoNome = x.Farmaco.Nome,
            };


        // GET: api/Medicamentos
        [HttpGet]
        public IEnumerable<MedicamentoDTO> GetMedicamento()
        {
            
            List<Apresentacao> apresentacoes = _context.Apresentacao.Include(f=>f.Farmaco).ToList();
            List<MedicamentoDTO> medicamentos = new List<MedicamentoDTO>();

            foreach (Apresentacao apr in apresentacoes)
            {

                int apresentacaoID = apr.Id;       
                List<Medicamento> meds = _context.Medicamento.Where(m => m.ApresentacaoId == apresentacaoID).ToList();

                foreach (Medicamento med in meds) {
                    medicamentos.Add(new MedicamentoDTO { id = med.id, Nome = med.Nome, apresentacao = new ApresentacaoDTO { Forma = med.Apresentacao.Forma, Quantidade = med.Apresentacao.Quantidade, Concentracao = med.Apresentacao.Concentracao, farmacoNome = med.Apresentacao.Farmaco.Nome } });
                }

            }
            return medicamentos.AsQueryable();
        }

        // GET: api/Medicamentos/nome={nome}
        [HttpGet("nome={nome}")]
        public IEnumerable<MedicamentoDTO> GetMedicamentoByName(string nome)
        {
             return _context.Medicamento.Where(n => n.Nome.Equals(nome)).Include(a=>a.Apresentacao).Select(AsMedicamentoDTO).ToList();
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.Include(f=>f.Apresentacao).Select(AsMedicamentoDTO).SingleOrDefaultAsync(m => m.id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(medicamento);
        }

        // GET: api/Medicamentos/{id}/Apresentacao
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetMedicamentoApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {

                return BadRequest(ModelState);
            }

            var idApresentacao = _context.Medicamento.Include(a => a.Apresentacao).Where(m => m.id == id).Select(x=> new ApresentacaoDTO { Forma = x.Apresentacao.Forma, Quantidade = x.Apresentacao.Quantidade, Concentracao = x.Apresentacao.Concentracao, farmacoNome = x.Apresentacao.Farmaco.Nome });

            if (idApresentacao == null)
            {
                return NotFound();
            }

            return Ok(idApresentacao);
        }

        // GET: api/Medicamentos/{id}/Posologias
        [HttpGet("{id}/Posologias")]
        public IQueryable<PosologiaDTO> GetPosologiasMedicamento([FromRoute] int id)
        {
            List<Apresentacao> apresentacoes = _context.Apresentacao.Include(f => f.Farmaco).ToList();
            List<Medicamento> medicamentos = _context.Medicamento.Include(m=>m.Apresentacao).Where(m => m.id == id).ToList();
            List<PosologiaDTO> posologias = new List<PosologiaDTO>();

            foreach (Medicamento med in medicamentos)
            {
                Farmaco far;
                int apresentacaoID = med.ApresentacaoId;
                Posologia posologia = _context.Posologia.SingleOrDefault(m=>m.ApresentacaoId==apresentacaoID);
                
                foreach (Apresentacao apresentacao in apresentacoes)
                {

                    if (apresentacao.Id == apresentacaoID)
                    {
                        far = apresentacao.Farmaco;
                        if (posologia != null)
                        {
                            posologias.Add(new PosologiaDTO { PosologiaHabitual = posologia.PosologiaHabitual, PosologiaMedico = posologia.PosologiaMedico, apresentacaoDTO = new ApresentacaoDTO { Forma = posologia.Apresentacao.Forma, Concentracao = posologia.Apresentacao.Concentracao, Quantidade = posologia.Apresentacao.Quantidade, farmacoNome = far.Nome } });

                        }
                    }
                }
                   
            }
            return posologias.AsQueryable();

        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] int id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.id)
            {
                return BadRequest();
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medicamento.Add(medicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamento", new { id = medicamento.id }, medicamento);
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamento.Remove(medicamento);
            await _context.SaveChangesAsync();

            return Ok(medicamento);
        }

        private bool MedicamentoExists(int id)
        {
            return _context.Medicamento.Any(e => e.id == id);
        }
    }
}