﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoARQSI.Models;
using Microsoft.AspNetCore.Authorization;

namespace ProjetoARQSI.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacoes")]
    public class ApresentacoesController : Controller
    {
        private readonly ProjetoARQSIContext _context;

        public ApresentacoesController(ProjetoARQSIContext context)
        {
            _context = context;
        }

        //Method to create DTO
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDTO
            = x => new ApresentacaoDTO
            {
                Forma = x.Forma,
                Quantidade = x.Quantidade,
                Concentracao = x.Concentracao,
                farmacoNome = x.Farmaco.Nome,
            };

        // GET: api/Apresentacoes
        [HttpGet]
        public IEnumerable<ApresentacaoDTO> GetApresentacao()
        {
            return _context.Apresentacao.Include(f=>f.Farmaco).Select(AsApresentacaoDTO);
        }

        // GET: api/Apresentacoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<ApresentacaoDTO> apresentacao =  _context.Apresentacao.Include(f=>f.Farmaco).Where(m => m.Id == id).Select(AsApresentacaoDTO).ToList();

            if (apresentacao.Count==0)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }

        // PUT: api/Apresentacoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacoes
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Apresentacao.Add(apresentacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApresentacao", new { id = apresentacao.Id }, apresentacao);
        }

        // DELETE: api/Apresentacoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacao.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return Ok(apresentacao);
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }
    }
}