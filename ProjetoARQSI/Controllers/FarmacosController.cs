﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoARQSI.Models;
using Microsoft.AspNetCore.Authorization;

namespace ProjetoARQSI.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly ProjetoARQSIContext _context;

        public FarmacosController(ProjetoARQSIContext context)
        {
            _context = context;
        }

        //Method to create DTO
        private static readonly Expression<Func<Farmaco, FarmacoDTO>> AsFarmacoDTO
            = x => new FarmacoDTO
            {
                Id = x.id,
                Nome = x.Nome,

            };


        //Method to create DTO
        private static readonly Expression<Func<Medicamento, MedicamentoDTO>> AsMedicamentoDTO
            = x => new MedicamentoDTO
            {
                id = x.id,
                Nome = x.Nome,

            };

        //Method to create DTO
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDTO
            = x => new ApresentacaoDTO
            {
                Forma = x.Forma,
                Quantidade = x.Quantidade,
                Concentracao = x.Concentracao,
                farmacoNome = x.Farmaco.Nome,
            };
        //Method to create DTO
        private static readonly Expression<Func<Posologia, PosologiaDTO>> AsPosologiaDTO
            = x => new PosologiaDTO
            {
                PosologiaHabitual = x.PosologiaHabitual,
                PosologiaMedico = x.PosologiaMedico,

            };
        // GET: api/Farmacos
        [HttpGet]
        public IEnumerable<FarmacoDTO> GetFarmaco()
        {
            return _context.Farmaco.Select(AsFarmacoDTO);
        }

        // GET: api/Farmacos/nome={nome}
        [HttpGet("nome={nome}")]
        public IEnumerable<FarmacoDTO> GetFarmacoByName(string nome)
        {
            return _context.Farmaco.Where(n => n.Nome.Equals(nome)).Select(AsFarmacoDTO);
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.Select(AsFarmacoDTO).SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }


        // GET: api/Farmacos/{id}/Medicamentos
        [HttpGet("{id}/Medicamentos")]
        public IQueryable<MedicamentoDTO> GetPosologiasMedicamento([FromRoute] int id)
        {

            List<Apresentacao> apresentacoes = _context.Apresentacao.Where(m => m.Id == id).ToList();
            List<MedicamentoDTO> medicamentos = new List<MedicamentoDTO>();

            foreach (Apresentacao apr in apresentacoes)
            {

                int apresentacaoID = apr.Id;
                Medicamento med = _context.Medicamento.SingleOrDefault(m => m.ApresentacaoId == apresentacaoID);
                if (med != null)
                {
                    medicamentos.Add(new MedicamentoDTO { id=med.id,Nome=med.Nome });
                }

            }
            return medicamentos.AsQueryable();

        }

        // GET: api/Farmacos/{id}/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetApresComFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var apresentacao = _context.Apresentacao.Include(a => a.Farmaco).Where(f => f.farmacoId == id).Select(AsApresentacaoDTO);

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }

        // GET: api/Farmacos/{id}/Posologias
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetPosFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var posologias = _context.Posologia.Include(a => a.Apresentacao).Where(f => f.Apresentacao.farmacoId == id).Select(AsPosologiaDTO);

            if (posologias == null)
            {
                return NotFound();
            }

            return Ok(posologias);
        }

        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.id)
            {
                return BadRequest();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmacoes
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.id }, farmaco);
        }

        // DELETE: api/Farmacoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            return Ok(farmaco);
        }

        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.id == id);
        }
    }
}