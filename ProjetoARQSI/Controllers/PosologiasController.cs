﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoARQSI.Models;
using Microsoft.AspNetCore.Authorization;
using System.Linq.Expressions;

namespace ProjetoARQSI.Controllers
{

    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Posologias")]
    public class PosologiasController : Controller
    {
        private readonly ProjetoARQSIContext _context;

        public PosologiasController(ProjetoARQSIContext context)
        {
            _context = context;
        }

        //Method to create DTO
        private static readonly Expression<Func<Posologia, PosologiaDTO>> AsPosologiaDTO
            = x => new PosologiaDTO
            {
                PosologiaHabitual = x.PosologiaHabitual,
                PosologiaMedico = x.PosologiaMedico,
                apresentacaoDTO = new ApresentacaoDTO { Forma = x.Apresentacao.Forma, Concentracao = x.Apresentacao.Concentracao, Quantidade = x.Apresentacao.Quantidade, farmacoNome = x.Apresentacao.Farmaco.Nome},
            };

        // GET: api/Posologias
        [HttpGet]
        public IEnumerable<PosologiaDTO> GetPosologia()
        {
            return _context.Posologia.Include(m=>m.Apresentacao).Select(AsPosologiaDTO);
        }

        // GET: api/Posologias/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<PosologiaDTO> posologia = _context.Posologia.Include(m=>m.Apresentacao).Where(m => m.Id == id).Select(AsPosologiaDTO).ToList();

            if (posologia.Count==0)
            {
                return NotFound();
            }

            return Ok(posologia);
        }

        // PUT: api/Posologias/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPosologia([FromRoute] int id, [FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologia.Id)
            {
                return BadRequest();
            }

            _context.Entry(posologia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Posologias
        [HttpPost]
        public async Task<IActionResult> PostPosologia([FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Posologia.Add(posologia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosologia", new { id = posologia.Id }, posologia);
        }

        // DELETE: api/Posologias/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.SingleOrDefaultAsync(m => m.Id == id);
            if (posologia == null)
            {
                return NotFound();
            }

            _context.Posologia.Remove(posologia);
            await _context.SaveChangesAsync();

            return Ok(posologia);
        }

        private bool PosologiaExists(int id)
        {
            return _context.Posologia.Any(e => e.Id == id);
        }
    }
}