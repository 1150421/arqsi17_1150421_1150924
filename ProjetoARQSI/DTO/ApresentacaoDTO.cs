﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class ApresentacaoDTO
    {

        public string Forma { get; set; }

        public string Quantidade { get; set; }

        public float Concentracao { get; set; }

       // public Farmaco farmaco { get; set;  }
       public string farmacoNome { get; set; }
    }
}
