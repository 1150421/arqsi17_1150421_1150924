﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class FarmacoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
