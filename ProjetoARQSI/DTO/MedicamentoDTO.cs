﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class MedicamentoDTO
    {
        public int id { get; set; }

        public string Nome { get; set; }
      
        public ApresentacaoDTO apresentacao { get; set; }

        
    }
}
