﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class PosologiaDTO
    {
        public string PosologiaHabitual { get; set; }

        public string PosologiaMedico { get; set; }

        public ApresentacaoDTO apresentacaoDTO { get; set; }
    }
}
