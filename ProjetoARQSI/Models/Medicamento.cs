﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class Medicamento
    {
        public int id { get; set; }

        public string Nome { get; set; }

        public int ApresentacaoId { get; set; }

        public Apresentacao Apresentacao { get; set; }


    }
}
