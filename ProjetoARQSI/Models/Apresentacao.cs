﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class Apresentacao
    {
        public int Id { get; set; }

        public string Forma { get; set; }

        public string Quantidade { get; set; }

        public float Concentracao { get; set; }

        public int farmacoId { get; set; }
        public Farmaco Farmaco { get; set; }


    }
}
