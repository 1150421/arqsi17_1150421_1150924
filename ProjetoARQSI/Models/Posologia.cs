﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoARQSI.Models
{
    public class Posologia
    {
        public int Id { get; set; }

        public string PosologiaHabitual { get; set; }

        public string PosologiaMedico { get; set; }

        public int ApresentacaoId { get; set; }
        public Apresentacao Apresentacao { get; set; }
        
    }
}
