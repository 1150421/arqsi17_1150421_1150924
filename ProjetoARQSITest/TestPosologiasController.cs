﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoARQSI.Controllers;
using ProjetoARQSI.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace ProjetoARQSITest
{
    [TestClass]
    public class TestPosologiasController
    {
        [TestMethod]
        public void GetPosologias()
        {
            var client = new HttpClient(); // no HttpServer

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:62559/api/posologias"),
                Method = HttpMethod.Get
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            using (var response = client.SendAsync(request).Result)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }


        private List<Posologia> GetTestMedicamentos()
        {
            var testProducts = new List<Posologia>();
            testProducts.Add(new Posologia { PosologiaHabitual = "PH1", PosologiaMedico = "PM1" });
            testProducts.Add(new Posologia { PosologiaHabitual = "PH2", PosologiaMedico = "PM2" });
            testProducts.Add(new Posologia { PosologiaHabitual = "PH3", PosologiaMedico = "PM3" });
            testProducts.Add(new Posologia { PosologiaHabitual = "PH4", PosologiaMedico = "PM4" });

            return testProducts;
        }
    }
}
