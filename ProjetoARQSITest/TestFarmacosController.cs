﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoARQSI.Controllers;
using ProjetoARQSI.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace ProjetoARQSITest
{
    [TestClass]
    public class TestFarmacosController
    {
        [TestMethod]
        public void GetFarmacos()
        {
            var client = new HttpClient(); // no HttpServer

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:62559/api/farmacos"),
                Method = HttpMethod.Get
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            using (var response = client.SendAsync(request).Result)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }


        private List<Farmaco> GetTestMedicamentos()
        {
            var testProducts = new List<Farmaco>();
            testProducts.Add(new Farmaco { id = 1, Nome = "F1" });
            testProducts.Add(new Farmaco { id = 2, Nome = "F2" });
            testProducts.Add(new Farmaco { id = 3, Nome = "F3" });
            testProducts.Add(new Farmaco { id = 4, Nome = "F4" });

            return testProducts;
        }
    }
}
