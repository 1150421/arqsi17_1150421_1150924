﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoARQSI.Controllers;
using ProjetoARQSI.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace ProjetoARQSITest
{
    [TestClass]
    public class TestApresentacoesController
    {
        [TestMethod]
        public void GetApresentacoes()
        {
            var client = new HttpClient(); // no HttpServer

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:62559/api/apresentacoes"),
                Method = HttpMethod.Get
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            using (var response = client.SendAsync(request).Result)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }


        private List<Apresentacao> GetTestMedicamentos()
        {
            var testProducts = new List<Apresentacao>();
            testProducts.Add(new Apresentacao { id = 1, Nome = "F1" });
            

            return testProducts;
        }
    }
}
