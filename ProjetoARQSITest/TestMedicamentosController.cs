﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoARQSI.Controllers;
using ProjetoARQSI.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace StoreApp.Tests
{
    [TestClass]
    public class TestMedicamentosController
    {
        [TestMethod]
        public void GetMedicamentos()
        {
            var client = new HttpClient(); // no HttpServer

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:62559/api/medicamentos"),
                Method = HttpMethod.Get
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            using (var response = client.SendAsync(request).Result)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }
       
        private List<Medicamento> GetTestMedicamentos()
        {
            var testProducts = new List<Medicamento>();
            testProducts.Add(new Medicamento { id = 1, Nome = "Demo1", ApresentacaoId = 1 });
            testProducts.Add(new Medicamento { id = 2, Nome = "Demo2", ApresentacaoId = 2 });
            testProducts.Add(new Medicamento { id = 3, Nome = "Demo3", ApresentacaoId = 3 });
            testProducts.Add(new Medicamento { id = 4, Nome = "Demo4", ApresentacaoId = 4 });

            return testProducts;
        }
    }
}
